from setuptools import find_packages
from setuptools import setup

version = '0.3'

setup(name='django-shibsso',
      version=version,
      description="Shibboleth (R) Authentication Backend for Django (R).",
      long_description="""\
Shibboleth (R) Authentication Backend for Django (R).""",
      classifiers=['Framework :: Django',
      'Programming Language :: Python',
      'Topic :: Software Development :: Libraries :: Python Modules',
      'Intended Audience :: Developers',
      'Operating System :: OS Independent',
      'License :: OSI Approved :: Apache Software License'], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='',
      author='CERN',
      author_email='',
      url='',
      license='Apache License (2.0)',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
      # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
